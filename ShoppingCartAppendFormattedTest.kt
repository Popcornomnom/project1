import org.junit.Before

import org.junit.Assert.*
import org.junit.Test

class ShoppingCartAppendFormattedTest {

    lateinit var shoppingCart: ShoppingCart
    lateinit var stringBuilder: StringBuilder

    @Before
    fun createShoppingCart() {
        shoppingCart = ShoppingCart()
        stringBuilder = StringBuilder()
    }

    @Test
    fun leftAlign() {
        shoppingCart.appendFormatted(stringBuilder, "Some Text", -1, 15)
        assertEquals("Some Text       ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "Some Text", -1, 20)
        assertEquals("Some Text            ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "Some Text", -1, 5)
        assertEquals("Some  ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "S", -1, 1)
        assertEquals("S ", stringBuilder.toString())
    }

    @Test
    fun rightAlign() {
        shoppingCart.appendFormatted(stringBuilder, "Some Text", 1, 15)
        assertEquals("      Some Text ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "Some Text", 1, 20)
        assertEquals("           Some Text ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "Some Text", 1, 5)
        assertEquals("Some  ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "S", 1, 1)
        assertEquals("S ", stringBuilder.toString())
    }

    @Test
    fun centerAlign() {
        shoppingCart.appendFormatted(stringBuilder, "Some Text", 0, 15)
        assertEquals("   Some Text    ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "Some Text", 0, 20)
        assertEquals("     Some Text       ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "Some Text", 0, 5)
        assertEquals("Some  ", stringBuilder.toString())
        stringBuilder.setLength(0)

        shoppingCart.appendFormatted(stringBuilder, "Some", 0, 1)
        assertEquals("S ", stringBuilder.toString())
    }

}