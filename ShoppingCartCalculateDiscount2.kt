import org.junit.Before

import org.junit.Assert.*
import org.junit.Test

class ShoppingCartCalculateDiscount2 {

    lateinit var shoppingCart: ShoppingCart

    @Before
    fun createShoppingCart() {
        shoppingCart = ShoppingCart()
    }

    // testcase 1
    @Test
    fun itemTypeNew() {
        assertEquals(0,shoppingCart.calculateDiscount(ShoppingCart.ItemType.NEW,1))
        assertEquals(0,shoppingCart.calculateDiscount(ShoppingCart.ItemType.NEW,10))
        assertEquals(0,shoppingCart.calculateDiscount(ShoppingCart.ItemType.NEW,100))
        assertEquals(0,shoppingCart.calculateDiscount(ShoppingCart.ItemType.NEW,10000))
    }


    // testcase 2
    @Test
    fun itemTypeSecondFree() {
        assertEquals(0,shoppingCart.calculateDiscount(
                ShoppingCart.ItemType.SECOND_FREE,1))
        assertEquals(50,shoppingCart.calculateDiscount(
                ShoppingCart.ItemType.SECOND_FREE,2))
    }

    // testcase 3

    @Test
    fun itemTypeSale() {
        assertEquals(70,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SALE,9))
    }

    //testcase 5
    @Test
    fun discountScaling() {
        assertEquals(0,shoppingCart.calculateDiscount(ShoppingCart.ItemType.NEW,100))
        assertEquals(50,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SECOND_FREE,9))
        assertEquals(55,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SECOND_FREE,59))
        assertEquals(80,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SECOND_FREE,10000))
    }
}