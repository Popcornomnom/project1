import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

/**
 * Containing items and calculating price.
 */
class ShoppingCart {
    enum class ItemType {
        NEW, REGULAR, SECOND_FREE, DISCOUNT, SALE
    }
    val maxItemsCount = 99

    /**
     * Adds new item.

     * @param title item title 1 to 32 symbols
     * *
     * @param price item price in USD, > 0
     * *
     * @param quantity item quantity, from 1
     * *
     * @param type item type
     * *
     * *
     * @throws IllegalArgumentException if some value is wrong
     */
    fun addItem(title: String?, price: Double, quantity: Int, type: ItemType) {
        if(items.count() == maxItemsCount)
            throw IndexOutOfBoundsException("Too much items!")
        items.add(Item(title, price, quantity, type))
    }

    /**
     * Formats shopping price.

     * @return string as lines, separated with \n,
     * * first line: # Item Price Quantity. Discount Total
     * * second line: ---------------------------------------------------------
     * * next lines: NN Title $PP.PP Q DD% $TT.TT
     * * 1 Some title $.30 2 - $.60
     * * 2 Some very long $100.00 1 50% $50.00
     * * ...
     * * 31 Item 42 $999.00 1000 - $999000.00
     * * end line: ---------------------------------------------------------
     * * last line: 31 $999050.60
     * *
     * * if no items in cart returns "No items." string.
     */
    fun formatTicket(): String {
        if (items.size == 0)
            return "No items."

        val lines = ArrayList<Array<String>>()
        val header = arrayOf("#", "Item", "Price", "Quantity", "Discount", "Total")
        val align = intArrayOf(1, -1, 1, 1, 1, 1)
        // formatting each line
        var total = 0.00
        var index = 0

        items.forEach {
            val discount = calculateDiscount(it.type, it.quantity)
            val itemTotal = it.price * it.quantity.toDouble() * (100.00 - discount) / 100.00

            lines.add(arrayOf((++index).toString(), it.title!!, MONEY.format(it.price), it.quantity.toString(),
                    if (discount == 0) "-" else discount.toString() + "%", MONEY.format(itemTotal)))
            total += itemTotal
        }

        val footer = arrayOf(index.toString(), "", "", "", "", MONEY.format(total))
        // formatting table

        // column max length
        val width = intArrayOf(0, 0, 0, 0, 0, 0)

        lines.forEach { line ->
            line.indices.forEach {
                width[it] = Math.max(width[it], line[it].length)
            }
        }

        footer.indices.forEach { width[it] = Math.max(width[it], footer[it].length) }

        // line length
        val lineLength = width.size - 1 + width.sum()

        val sb = StringBuilder()
        // header
        for (i in header.indices)
            appendFormatted(sb, header[i], align[i], width[i])
        sb.append("\n")
        // separator
        for (i in 0..lineLength - 1)
            sb.append("-")
        sb.append("\n")
        // lines

        lines.forEach { line->
            line.indices.forEach { appendFormatted(sb, line[it], align[it], width[it]) }
            sb.append("\n")
        }

        if (lines.size > 0) {
            // separator
            for (i in 0..lineLength - 1)
                sb.append("-")
            sb.append("\n")
        }
        // footer
        footer.indices.forEach { appendFormatted(sb, footer[it], align[it], width[it]) }

        return sb.toString()
    }

    /** item info  */
    private class Item(var _title: String? , var _price: Double, var _quantity: Int, var type: ItemType) {
        var title: String?
            get() = _title
            set(value) {
                if (value == null || value.length === 0 || value.length > 32)
                    throw IllegalArgumentException("Illegal title")
                _title = value
            }

        var price: Double
        get() = _price
        set(value) {
            if (value < 0.01 || value > 1000)
                throw IllegalArgumentException("Illegal price")
            _price = value
        }

        var quantity: Int
        get() = _quantity
        set(value) {
            if (value <= 0 || value > 1000)
                throw IllegalArgumentException("Illegal quantity")
            _quantity = value
        }

        init {
            title = _title
            price = _price
            quantity = _quantity
        }
    }

    /** Container for added items  */
    private val items = ArrayList<Item>()

        // --- private section -----------------------------------------------------
        private val MONEY: NumberFormat

        init {
            val symbols = DecimalFormatSymbols()
            symbols.decimalSeparator = '.'
            MONEY = DecimalFormat("$#.00", symbols)
        }

        /**
         * Appends to sb formatted value.
         * Trims string if its length > width.
         * @param align -1 for align left, 0 for center and +1 for align right.
         */
        fun appendFormatted(sb: StringBuilder, value: String, align: Int, width: Int) {
            var tempValue = value
            if (tempValue.length > width)
                tempValue = tempValue.substring(0, width)
            var before = if (align == 0)
                (width - tempValue.length) / 2
            else if (align == -1) 0 else width - tempValue.length
            var after = width - tempValue.length - before
            while (before-- > 0)
                sb.append(" ")
            sb.append(tempValue)
            while (after-- > 0)
                sb.append(" ")
            sb.append(" ")
        }

        /**
         * Calculates item's discount.
         * For NEW item discount is 0%;
         * For SECOND_FREE item discount is 50% if quantity > 1
         * For SALE item discount is 70%
         * For each full 10 not NEW items item gets additional 1% discount,
         * but not more than 80% total
         */
        fun calculateDiscount(type: ItemType, quantity: Int): Int {
            var discount = 0
            when (type) {
                ShoppingCart.ItemType.NEW -> return 0

                ShoppingCart.ItemType.REGULAR -> discount = 0

                ShoppingCart.ItemType.SECOND_FREE -> if (quantity > 1)
                    discount = 50

                ShoppingCart.ItemType.SALE -> discount = 70
                else -> {
                    throw IllegalArgumentException("Bad discount type")
                }
            }

            if (discount < 80) {
                discount += (quantity / 100)*10
                if (discount > 80)
                    discount = 80
            }

            return discount
        }

}