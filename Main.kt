/**
 * Created by Rita on 12/18/16.
 */
/**
 * Tests all class methods.
 */
fun main(args: Array<String>) {
    // TODO: add tests here
    val cart = ShoppingCart()
    cart.addItem("Apple", 0.99, 5, ShoppingCart.ItemType.NEW)
    cart.addItem("Banana", 20.00, 4, ShoppingCart.ItemType.SECOND_FREE)
    cart.addItem("A long piece of toilet paper", 17.20, 1, ShoppingCart.ItemType.SALE)
    cart.addItem("Nails", 2.00, 500, ShoppingCart.ItemType.REGULAR)
    System.out.println(cart.formatTicket())
}

