import org.junit.Before

import org.junit.Assert.*
import org.junit.Test

class ShoppingCartCalculateDiscount {

    lateinit var shoppingCart: ShoppingCart

    @Before
    fun createShoppingCart() {
        shoppingCart = ShoppingCart()
    }

    // testcase 1
    @Test
    fun itemTypeRegular() {
        assertEquals(0,shoppingCart.calculateDiscount(ShoppingCart.ItemType.REGULAR,1))
    }

    // testcase 2
    @Test
    fun itemTypeSecondYes() {
        assertEquals(50,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SECOND_FREE,2))
    }
    @Test
    fun itemTypeSecondNo() {
        assertEquals(0,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SECOND_FREE,1))
    }

    // testcase 3
    @Test
    fun itemTypeDiscountStart() {
        assertEquals(10,shoppingCart.calculateDiscount(ShoppingCart.ItemType.DISCOUNT,1))
    }
    @Test
    fun itemTypeDiscountHalf() {
        assertEquals(30,shoppingCart.calculateDiscount(ShoppingCart.ItemType.DISCOUNT,20))
    }
    @Test
    fun itemTypeDiscountFull() {
        assertEquals(50,shoppingCart.calculateDiscount(ShoppingCart.ItemType.DISCOUNT,70))
    }

    //testcase 4
    @Test
    fun itemTypeSale() {
        assertEquals(90,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SALE,70))
    }

    //testcase 5
    @Test
    fun discountScaling() {
        assertEquals(70,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SECOND_FREE,270))
        assertEquals(80,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SECOND_FREE,970))
        assertEquals(50,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SECOND_FREE,99))
        assertEquals(90,shoppingCart.calculateDiscount(ShoppingCart.ItemType.SALE,9999))
    }
}