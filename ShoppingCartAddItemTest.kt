import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class ShoppingCartAddItemTest {
    lateinit var shoppingCart: ShoppingCart

    @Before
    fun createShoppingCart() {
        shoppingCart = ShoppingCart()
    }


    // testcase 1
    @Test(expected = IllegalArgumentException::class)
    fun nullTitle() {
        shoppingCart.addItem(null,0.2,1,ShoppingCart.ItemType.NEW)
    }
    @Test(expected = IllegalArgumentException::class)
    fun emptyTitle() {
        shoppingCart.addItem("",0.2,1,ShoppingCart.ItemType.NEW)
    }

    @Test(expected = IllegalArgumentException::class)
    fun tooLongTitle() {
        shoppingCart.addItem("123456789123456789123456789123456789123456789123456789",0.2,1,ShoppingCart.ItemType.NEW)
    }

    // testcase 2
    @Test(expected = IllegalArgumentException::class)
    fun negativePrice() {
        shoppingCart.addItem("Some title",-1.0,1,ShoppingCart.ItemType.NEW)
    }
    @Test(expected = IllegalArgumentException::class)
    fun nearZeroPrice() {
        shoppingCart.addItem("Some title",.0009,1,ShoppingCart.ItemType.NEW)
    }
    @Test(expected = IllegalArgumentException::class)
    fun tooBigPrice() {
        shoppingCart.addItem("Some title",1000.1,1,ShoppingCart.ItemType.NEW)
    }

    //testcase 3
    @Test(expected = IllegalArgumentException::class)
    fun negativeQuantity() {
        shoppingCart.addItem("Some title",1.0, Int.MIN_VALUE,ShoppingCart.ItemType.NEW)
    }
    @Test(expected = IllegalArgumentException::class)
    fun zeroQuantity() {
        shoppingCart.addItem("Some title",1.0, 0,ShoppingCart.ItemType.NEW)
    }
    @Test(expected = IllegalArgumentException::class)
    fun tooBigQuantity() {
        shoppingCart.addItem("Some title",1.0, 1001,ShoppingCart.ItemType.NEW)
    }


    //testcase 4 impossible to broke

    //testcase 5
    @Test(expected = IndexOutOfBoundsException::class)
    fun add100Items() {
        for(i in 1..100) {
            shoppingCart.addItem("some item title"+i, i.toDouble(), i, ShoppingCart.ItemType.NEW)
        }
    }

    // testcase 6 correct?


    @Test(expected = IllegalArgumentException::class)
    fun allWrongParams() {
        shoppingCart.addItem("",0.0,0,ShoppingCart.ItemType.NEW)
    }

    //Positive test
    @Test
    fun nanPrice() {
        shoppingCart.addItem("Some title",Double.NaN,1,ShoppingCart.ItemType.NEW)
    }
}